from distutils.command.clean import clean
from aiogram import Bot, Dispatcher, executor, types
from config import token, user_id
from aiogram.utils.markdown import hbold, hunderline, hcode, hlink
from pic4a import check_news_update
from aiogram.dispatcher.filters import Text  # для привязки hendler к кнопкам
import json
import asyncio

bot = Bot(token=token, parse_mode=types.ParseMode.HTML) #объект бота
dp = Dispatcher(bot) # объект для управление handler

@dp.message_handler(commands="start") # commands - команда, которую мы отпрвляем боту
async def start(message: types.Message): # функция для создания и демонстрации пользователь кнопок
    start_buttons = ["все новости", "5 последних новостей", "свежие новости"]
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True) # создаем объект клавиатуры , resize_keyboard=True - отвечает за размер кнопки
    keyboard.add(*start_buttons) # добавляем в объект клавиатуры список кнопок

    await message.answer("лента новостей", reply_markup=keyboard)

@dp.message_handler(Text(equals="все новости")) # Text для привязки hendler к кнопкам
async def get_all_news(message: types.Message):
    with open("news_dict.json") as file: # подгружаем словарь из json файла
        news_dict = json.load(file)
    
    # цикл для отбора новостей и передачи их пользователю
    for key, value in news_dict.items(): # hbold, hlink, hcode, hunderline -  методы для форматирования 
        news = f"{hbold(value['time_new'])}\n" \
               f"{hlink(value['title_new'], value['url_new'])}"
               #f"{hunderline(value['title_new'])}\n" \
               #f"{hcode(value['desc_new'])}\n"  \
        await message.answer(news)

@dp.message_handler(Text(equals="5 последних новостей"))
async def get_last_five_news(message: types.Message): #получение последних 5 новостей
    with open("news_dict.json") as file:
        news_dict = json.load(file)

    for key, value in sorted(news_dict.items())[-5:]:
        news = f"{hbold(value['time_new'])}\n" \
               f"{hlink(value['title_new'], value['url_new'])}"
               
        await message.answer(news)

@dp.message_handler(Text(equals="свежие новости"))
async def get_fresh_news(message: types.Message):
    fresh_news = check_news_update()

    if len(fresh_news) >= 1:
        for key, value in fresh_news.items():
            news = f"{hbold(value['time_new'])}\n" \
                   f"{hlink(value['title_new'], value['url_new'])}"
            
            await message.answer(news)
        fresh_news.clear()
    else:
        await message.answer("Пока нет свежих новостей...")

async def chech_news_every_minute(): # функция проверяет свежие новости раз в минуту
    while True:
        fresh_news = check_news_update()
        if len(fresh_news) >= 1:
            for key, value in fresh_news.items():
                news = f"{hbold(value['time_new'])}\n" \
                    f"{hlink(value['title_new'], value['url_new'])}"
                
                #на определенный id отправляем свежие новости (id можно получить userinfobot)
                await bot.send_message(user_id, news, disable_notification=True)  #disable_notification для отправки сообщений в безвучном режиме
            fresh_news.clear()
        else:
            await bot.send_message(user_id, "пока нет свежих новостей!!")

        await asyncio.sleep(60) # время ожидания

if __name__ == "__main__":
    loop = asyncio.get_event_loop() # создаем петлю для возвращения текущего цикла событий
    loop.create_task(chech_news_every_minute()) # создаем задачу 
    executor.start_polling(dp)
