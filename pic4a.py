

import json
import requests
from bs4 import BeautifulSoup
import fake_useragent

user = fake_useragent.UserAgent().random

news_dict = {} #словарь со всеми новостями!
fresh_news = {}  # словарь с новыми новостями!!!

def get_news(): #получение всех новостей
    headers = {
        "user_agent":user
    }
    url = "https://www.kommersant.ru/theme/1225"
    responce = requests.get(url, headers=headers).text
    soup = BeautifulSoup(responce, "lxml")
    block_news = soup.find("div", class_="rubric")
    news = block_news.find_all("article", class_="uho rubric_lenta__item js-article")
    return news

def get_info_news(new): #получение "параметров" новости в виде словаря
    title_new = new.find("a", class_="uho__link uho__link--overlay").text #название новости
    try:
        desc_new = new.find("h3", class_="uho__subtitle rubric_lenta__item_subtitle").find("a", class_="uho__link").text #описание новости
    except AttributeError:
        desc_new = "-"
    url_new = f'https://www.kommersant.ru{new.find("a").get("href")}' #ссылка на новость
    id_new = url_new.split("/")[-1] #последнее значение каждой сслыки, ведущей на новость, будем использовать как ключ в словаре
    time_new = new.find("p", class_="uho__tag rubric_lenta__item_tag hide_mobile").text.strip()
    return {"title_new":title_new, "url_new":url_new, "id_new":id_new , "time_new":time_new, "desc_new":desc_new}

def add_news_in_dict(my_dict, new): #добавление новости в словарь 
    my_dict[get_info_news(new)["id_new"]] = {
        "time_new" : get_info_news(new)["time_new"],
        "title_new" : get_info_news(new)["title_new"],
        "desc_new" : get_info_news(new)["desc_new"],
        "url_new" : get_info_news(new)["url_new"]
    }

def record_news_in_json(): # запись новостей из словаря в json  файл
    for new in get_news():
        add_news_in_dict(news_dict, new)
    with open("/home/roma/code/pars/news_dict.json", "w") as file:
        json.dump(news_dict, file, indent=4, ensure_ascii=False)


def check_news_update(): # проверяет появились ли новые новости и возвращает их в иде словаря
    with open("/home/roma/code/pars/news_dict.json") as file:
        news_dict = json.load(file)
    for new in get_news():
        url_new = f'https://www.kommersant.ru{new.find("a").get("href")}' #ссылка на новость
        id_new = url_new.split("/")[-1] #последнее значение каждой сслыки, ведущей на новость, будем использовать как ключ в словаре

        if id_new in news_dict: # проверяем наличие всеx id новостей на сайте с нашими id ( которые мы уже прочитали), если все id совпадают, значит новых новостей нет, иначе мы добавляем новость и с новым id в наш словарь new_dict
            continue
        else:
            add_news_in_dict(news_dict, new)
            add_news_in_dict(fresh_news, new)
    with open("/home/roma/code/pars/news_dict.json", "w") as file:    #перезаписываем данные словаря со всеми добавленными новостями
        json.dump(news_dict, file, indent=4, ensure_ascii=False)
    return fresh_news  #возвращаем последние новости


def main():
    #get_first_news()   #данная функция нужна только один раз, при первом запуске программы, чтоб сохранить все последние новости в словарь, который будет отправной точкой для добавления новых новостей
    print(check_news_update())

if __name__ == "__main__":
    main()

